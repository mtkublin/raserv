from mongo_utils import mongo_get
from rasa_ops import train_model, test_with_model
import requests


TRAINING_DOCS = {}
TEST_DOCS = {}


def train_receive_mongoid(train_data_id, project_name, model_name):

    req_id = train_data_id["DATA"]["req_id"]


    if "model_path" in train_data_id["DATA"].keys():
        model_path = train_data_id["DATA"]["model_path"]
        data_id = train_data_id["DATA"]["data_id"]
        f = open(model_path + "\\TRAIN_DATA\\" + data_id + ".json", "r")
        train_doc = eval(f.read())
        f.close()
        train_model(train_doc, project_name, model_name, model_path)

    else:
        mongo_id = train_data_id["DATA"]["mongo_id"]
        doc = mongo_get(mongo_id)
        TRAINING_DOCS[mongo_id] = doc["rasa_nlu_data"]
        train_doc = {"rasa_nlu_data": TRAINING_DOCS[mongo_id]}
        train_model(train_doc, project_name, model_name)

    r = requests.put(url="http://127.0.0.1:6000/api/traindata/" + str(req_id))
    print(r)

    return req_id


def train_read_all():
    return [TRAINING_DOCS[key] for key in sorted(TRAINING_DOCS.keys())]


# TEST ----------------------------------------------------------------------------------------------------------------


def test_receive_data(test_data):
    # mongo_id = test_data["DATA"]["mongo_id"]
    # doc = mongo_get(mongo_id, coll_n="unprocessed_test_data")
    # result_mongo_id = mongo_import(sample_test_result, coll_n="test_results")
    # data_to_send = {"req_id": req_id, "mongo_id": result_mongo_id}

    req_id = test_data["DATA"]["req_id"]
    doc = test_data["DATA"]["test_data"]
    TEST_DOCS[req_id] = {"req_id": req_id,
                         "SENTS": doc["SENTS"]}
    r = requests.put(url="http://127.0.0.1:6000/api/testdata/" + str(req_id))
    print(r)

    print("Proceeding to test")
    result_data = test_with_model(TEST_DOCS[req_id]["SENTS"])

    data_to_send = {"req_id": req_id,
                    "result": result_data}
    r2 = requests.post(url="http://127.0.0.1:6000/api/testdata/" + str(req_id),
                       json={"DATA": data_to_send})
    print(r2)
    return str(r2)


def test_read_all():
    return [TEST_DOCS[key] for key in sorted(TEST_DOCS.keys())]

