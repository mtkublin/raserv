from rasa_nlu.model import Trainer
from rasa_nlu.model import Interpreter
from rasa_nlu.training_data.formats import RasaReader
from rasa_nlu.persistor import AzurePersistor
from rasa_nlu import config
import warnings
import os
import shutil

warnings.filterwarnings(module='h5py*', action='ignore', category=FutureWarning)

persistor = AzurePersistor(azure_container= 'rasa-models-test-container', azure_account_name= 'csb6965281488a3x4dd7xbcd',
                           azure_account_key= 'ZT4DVNgFfQrbglJXzUX2HuPi6KYysL3b2zxdNlL11umXg811fptnTcubKQ83itTLDAmSdmfqgpiJWylbfumTDQ==')


interpreter_dict = {"current_project": "", "current_model": "", "interpreters": {}}


def load_interpreter(model_name, project_name, force):
    if force == "True":
        persistor.retrieve(model_name=model_name, project=project_name, target_path='.\\temp_test_model')

        rm_path = '.\\' + project_name + '___' + model_name + '.tar.gz'
        os.remove(rm_path)

        print("Loading interpreter")
        interpreter = Interpreter.load(".\\temp_test_model")
        warnings.filterwarnings(module='sklearn*', action='ignore', category=DeprecationWarning)
        warnings.filterwarnings(module='rasa_nlu*', action='ignore', category=UserWarning)
        print("Loaded")

        shutil.rmtree('.\\temp_test_model')

        interpreter_dict["interpreters"][project_name][model_name] = interpreter

    if project_name not in interpreter_dict["interpreters"].keys() or model_name not in interpreter_dict["interpreters"][project_name].keys():

        persistor.retrieve(model_name=model_name, project=project_name, target_path='.\\temp_test_model')

        rm_path = '.\\' + project_name + '___' + model_name + '.tar.gz'
        os.remove(rm_path)

        print("Loading interpreter")
        interpreter = Interpreter.load(".\\temp_test_model")
        warnings.filterwarnings(module='sklearn*', action='ignore', category=DeprecationWarning)
        warnings.filterwarnings(module='rasa_nlu*', action='ignore', category=UserWarning)
        print("Loaded")

        shutil.rmtree('.\\temp_test_model')

        if project_name not in interpreter_dict["interpreters"].keys():
            interpreter_dict["interpreters"][project_name] = {model_name: interpreter}
        else:
            interpreter_dict["interpreters"][project_name][model_name] = interpreter

        interpreter_dict["current_project"] = project_name
        interpreter_dict["current_model"] = model_name

    interpreter_dict["current_project"] = project_name
    interpreter_dict["current_model"] = model_name


def load_interpreter_local(model_name, project_name, force, model_path):

    new_model_path = model_path["DATA"]
    print(new_model_path)

    if force == "True":
        print("Loading interpreter")
        interpreter = Interpreter.load(new_model_path)
        warnings.filterwarnings(module='sklearn*', action='ignore', category=DeprecationWarning)
        warnings.filterwarnings(module='rasa_nlu*', action='ignore', category=UserWarning)
        print("Loaded")

        interpreter_dict["interpreters"][project_name][model_name] = interpreter

    if project_name not in interpreter_dict["interpreters"].keys() or model_name not in interpreter_dict["interpreters"][project_name].keys():

        print("Loading interpreter")
        interpreter = Interpreter.load(new_model_path)
        warnings.filterwarnings(module='sklearn*', action='ignore', category=DeprecationWarning)
        warnings.filterwarnings(module='rasa_nlu*', action='ignore', category=UserWarning)
        print("Loaded")

        if project_name not in interpreter_dict["interpreters"].keys():
            interpreter_dict["interpreters"][project_name] = {model_name: interpreter}
        else:
            interpreter_dict["interpreters"][project_name][model_name] = interpreter

        interpreter_dict["current_project"] = project_name
        interpreter_dict["current_model"] = model_name

    interpreter_dict["current_project"] = project_name
    interpreter_dict["current_model"] = model_name


def train_model(json_data, proj_name, mod_name, model_path = None):
    reader = RasaReader()
    training_data = reader.read_from_json(json_data)
    trainer = Trainer(config.load(".\\config.yml"))
    trainer.train(training_data)
    if model_path == None:
        os.mkdir('.\\temp_train_model')
        trainer.persist(path='.\\temp_train_model', persistor=persistor, project_name= proj_name, fixed_model_name= mod_name)
        shutil.rmtree('.\\temp_train_model')
    else:
        trainer.persist(path=model_path + "\\MODELS", project_name= proj_name, fixed_model_name= mod_name)


def test_with_model(sents_list_to_test):
    cur_proj = interpreter_dict["current_project"]
    cur_mod = interpreter_dict["current_model"]
    interpreter = interpreter_dict["interpreters"][cur_proj][cur_mod]

    res_list = []
    for text_to_test in sents_list_to_test:
        result = interpreter.parse(text_to_test)
        res_list += [result]

    return res_list


def get_projects_list():
    projects_list = persistor.list_projects()
    return projects_list


def get_models_list(project):
    models_list = persistor.list_models(project)
    return models_list
